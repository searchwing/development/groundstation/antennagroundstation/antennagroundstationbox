# AntennaGroundStationBox

Hardware, Software and System for the antenna groundstation box. This is the box that contains the RFDesign RFD868+ modem, the UART to Ethernet Converter and the Wifi Access Point.

See: [Wiki Antenna Bround Box](https://www.hs-augsburg.de/homes/beckmanf/dokuwiki/doku.php?id=searchwing-antenna-ground-box)
